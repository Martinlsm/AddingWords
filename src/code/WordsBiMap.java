package code;

import java.util.Optional;
import java.util.TreeMap;

public class WordsBiMap {

    private TreeMap<Integer, String> intToString = new TreeMap<>();
    private TreeMap<String, Integer> stringToInt = new TreeMap<>();

    public void defWord(String word, int value) {
        valueOfWord(word).map(x -> intToString.remove(x));
        findWord(value).map(x -> stringToInt.remove(x));
        intToString.put(value, word);
        stringToInt.put(word, value);
    }

    public Optional<Integer> valueOfWord(String word) {
        return Optional.ofNullable(stringToInt.get(word));
    }

    public Optional<String> findWord(int value) {
        return Optional.ofNullable(intToString.get(value));
    }

    public void clear() {
        intToString = new TreeMap<>();
        stringToInt = new TreeMap<>();
    }
}
