package code;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class ArithmeticExpression {

    private static final String UNKNOWN = "unknown";

    private List<ArithmeticEntry> expression = new LinkedList<>();

    public void addEntry(String word, boolean positive) {
        expression.add(new ArithmeticEntry(word, positive));
    }

    public String eval(WordsBiMap biMap) {
        int sum = 0;
        for (ArithmeticEntry e : expression) {
            Optional<Integer> val = e.apply(biMap);
            if (!val.isPresent()) {
                return UNKNOWN;
            } else {
                sum += e.apply(biMap).get();
            }
        }
        return biMap.findWord(sum).orElse(UNKNOWN);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (ArithmeticEntry e : expression) {
            sb.append(e).append(" ");
        }
        if (sb.charAt(0) == '+') {
            sb.delete(0, 2);
        }
        sb.delete(sb.length() - 1, sb.length());
        return sb.toString();
    }

    private class ArithmeticEntry {

        private boolean positive;
        private String word;

        public ArithmeticEntry(String word, boolean positive) {
            this.word = word;
            this.positive = positive;
        }

        public Optional<Integer> apply(WordsBiMap biMap) {
            if (!positive) {
                return biMap.valueOfWord(word).map(x -> -x);
            } else {
                return biMap.valueOfWord(word);
            }
        }

        public String toString() {
            return (positive ? "+ " : "- ") + word;
        }
    }

}

