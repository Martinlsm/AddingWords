package code;

import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
        WordsBiMap biMap = new WordsBiMap();
        Scanner scan = new Scanner(System.in);
        while (scan.hasNext()) {
            processInput(biMap, scan.nextLine().trim()).ifPresent(x -> System.out.println(x));
        }
        scan.close();
    }

    public static Optional<String> processInput(WordsBiMap biMap, String line) {
            int argumentEnd = line.indexOf(" ");
            String argument = line.substring(0, (argumentEnd < 0 ? line.length() : argumentEnd));
            String[] split;
            switch (argument) {
                case "def" :
                    split = line.split(" ");
                    biMap.defWord(split[1], Integer.parseInt(split[2]));
                    return Optional.empty();
                case "calc" :
                    if (!line.endsWith(" =")) {
                        throw new RuntimeException("Cannot parse calc-expression. Line needs to end with \" =\".");
                    }
                    ArithmeticExpression expression = new ArithmeticExpression();
                    split = line.split(" ");
                    fillExpression(Arrays.copyOfRange(split, 1, split.length - 1), expression);
                    return Optional.of(expression.toString() + " = "  + expression.eval(biMap));
                case "clear" :
                    biMap.clear();
                    return Optional.empty();
            }
            return Optional.empty();
    }

    private static void fillExpression(String[] array, ArithmeticExpression expr) {
        int start = 0;
        if (!array[0].equals("-")) {
            expr.addEntry(array[0], true);
            start = 1;
        }
        for (int i = start; i < array.length; i = i + 2) {
            if (array[i].equals("+")) {
                expr.addEntry(array[i + 1], true);
            } else if (array[i].equals("-")) {
                expr.addEntry(array[i + 1], false);
            } else {
                throw new RuntimeException("Cannot parse string. Operator needs to be one of { \"+\", \"-\" } but was: " + array[i]);
            }
        }
    }
}
