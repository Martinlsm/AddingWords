package test;

import code.ArithmeticExpression;
import code.WordsBiMap;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestArithmeticExpression {

    private WordsBiMap biMap;
    private ArithmeticExpression expression;

    @Before
    public void setup() {
        biMap = new WordsBiMap();
        expression = new ArithmeticExpression();
        biMap.defWord("Alligator", 1);
        biMap.defWord("Baboon", 2);
        biMap.defWord("Catfish", 3);
    }

    @Test
    public void testOneExistingArgument() {
        expression.addEntry("Alligator", true);
        assertEquals("Alligator", expression.toString(), expression.eval(biMap));
    }

    @Test
    public void testOneUnknownArgument() {
        expression.addEntry("Dolphin", true);
        assertEquals("Dolphin", expression.toString());
        assertEquals("unknown", expression.eval(biMap));
    }

    @Test
    public void testPlus() {
        expression.addEntry("Alligator", true);
        expression.addEntry("Baboon", true);
        assertEquals("Catfish", expression.eval(biMap));
        assertEquals("Alligator + Baboon", expression.toString());
    }

    @Test
    public void testMinus1() {
        expression.addEntry("Alligator", false);
        expression.addEntry("Baboon", true);
        assertEquals("Alligator", expression.eval(biMap));
        assertEquals("- Alligator + Baboon", expression.toString());
    }

    @Test
    public void testMinus2() {
        expression.addEntry("Catfish", true);
        expression.addEntry("Alligator", false);
        assertEquals("Baboon", expression.eval(biMap));
        assertEquals("Catfish - Alligator", expression.toString());
    }

    @Test
    public void testUnknownAnswer() {
        expression.addEntry("Catfish", true);
        expression.addEntry("Alligator", true);
        assertEquals("Catfish + Alligator", expression.toString());
        assertEquals("unknown", expression.eval(biMap));
    }

    @Test
    public void testUnknownTerm() {
        expression.addEntry("Dolphin", false);
        expression.addEntry("Baboon", false);
        assertEquals("- Dolphin - Baboon", expression.toString());
        assertEquals("unknown", expression.eval(biMap));
    }

    @Test
    public void testNegativeAnswer() {
        expression.addEntry("Baboon", true);
        expression.addEntry("Catfish", false);
        assertEquals("Baboon - Catfish", expression.toString());
        assertEquals("unknown", expression.eval(biMap));
    }

    @Test
    public void testNegativeUnknownAnswer() {
        expression.addEntry("Alligator", false);
        expression.addEntry("Catfish", false);
        assertEquals("unknown", expression.eval(biMap));
    }
}
