package test;

import code.WordsBiMap;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.*;

public class TestWordsBiMap {

    private WordsBiMap biMap;

    @Before
    public void setup() {
        biMap = new WordsBiMap();
    }

    @Test
    public void simpleInsert() {
        biMap.defWord("banana", 42);
        assertEquals(Optional.of("banana"), biMap.findWord(42));
        assertEquals(Optional.of(42), biMap.valueOfWord("banana"));
    }

    @Test
    public void testUnknown() {
        assertEquals(Optional.empty(), biMap.findWord(666));
    }

    @Test
    public void testClear() {
        biMap.defWord("foo", 100);
        assertEquals("foo", biMap.findWord(100).get());

        biMap.clear();
        assertEquals(Optional.empty(), biMap.valueOfWord("foo"));
        assertEquals(Optional.empty(), biMap.findWord(100));

        biMap.defWord("coffee", 100);
        assertEquals(Optional.of("coffee"), biMap.findWord(100));
        assertEquals(Optional.of(100), biMap.valueOfWord("coffee"));
    }

    @Test
    public void testOverwriteWord() {
        biMap.defWord("pikachu", 10);
        biMap.defWord("charmander", 10);
        assertEquals(Optional.of("charmander"), biMap.findWord(10));
        assertEquals(Optional.empty(), biMap.valueOfWord("pikachu"));
        assertEquals(Optional.of(10), biMap.valueOfWord("charmander"));
    }

    @Test
    public void testOverwriteNumber() {
        biMap.defWord("godzilla", 33);
        biMap.defWord("godzilla", 66);
        assertEquals(Optional.empty(), biMap.findWord(33));
        assertEquals(Optional.of("godzilla"), biMap.findWord(66));
        assertEquals(Optional.of(66), biMap.valueOfWord("godzilla"));
    }

    @Test
    public void testSomeInserts() {
        biMap.defWord("A", 1);
        biMap.defWord("B", 2);
        biMap.defWord("C", 3);
        assertEquals(Optional.of("A"), biMap.findWord(1));
        assertEquals(Optional.of(1), biMap.valueOfWord("A"));
        assertEquals(Optional.of("B"), biMap.findWord(2));
        assertEquals(Optional.of(2), biMap.valueOfWord("B"));
        assertEquals(Optional.of("C"), biMap.findWord(3));
        assertEquals(Optional.of(3), biMap.valueOfWord("C"));
    }

}
