package test;


import code.Main;
import code.WordsBiMap;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Optional;
import java.util.Scanner;

import static org.junit.Assert.*;

public class TestParsing {

    private final static String TEST_FILE_PATH = System.getProperty("user.dir").replace("\\", "/") + "/TestFiles";

    WordsBiMap biMap;

    @Before
    public void setup() {
        biMap = new WordsBiMap();
    }

    @Test
    public void compareInAndOutFiles() throws FileNotFoundException {
        File folder = new File(TEST_FILE_PATH + "/Input");
        String[] inputFiles = folder.list();
        for (String inputFile : inputFiles) {
            System.out.println("Testing: " + inputFile);
            int strIndex = inputFile.indexOf(".txt");
            String outputFile = inputFile.substring(0, strIndex) + "-out.txt";

            Scanner scanIn = new Scanner(new File(TEST_FILE_PATH + "/Input/" + inputFile));
            Scanner scanOut = new Scanner(new File(TEST_FILE_PATH + "/ExpectedOutput/" + outputFile));

            while (scanIn.hasNext()) {
                Optional<String> opt = Main.processInput(biMap, scanIn.nextLine().trim());
                opt.ifPresent(x -> assertEquals(scanOut.nextLine(), x));
            }
            scanIn.close();
            scanOut.close();
        }

    }
}
